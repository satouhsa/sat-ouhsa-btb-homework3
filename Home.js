import React, { Component } from 'react'
import { Text, View } from 'react-native'


const show = ()=>{
    return(
      <Container>
      <Header />
      <Content>
        <Card>
          <CardItem>
            <Left>
              {/* <Thumbnail source={{uri: 'Image URL'}} /> */}
              <Body>
                <Text>NativeBase</Text>
                <Text note>GeekyAnts</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
          
          </CardItem>
          <CardItem>
            <Left>
              <Button transparent>
                {/* <Icon active name="thumbs-up" /> */}
                <Text>12 Likes</Text>
              </Button>
            </Left>
            <Body>
              <Button transparent>
                {/* <Icon active name="chatbubbles" /> */}
                <Text>4 Comments</Text>
              </Button>
            </Body>
            {/* <Right>
              <Text>11h ago</Text>
            </Right> */}
          </CardItem>
        </Card>
      </Content>
    </Container>

    );
}
 class Home extends Component {

    constructor(){
        super();
        this.state={
            title:"",
            desc:""
        }
    }
    render() {
        return (
            <Container>
            <Header><Text style={{marginTop:20,fontWeight:'bold'}}>Headers</Text></Header>
              <Text style={{fontWeight:'bold',fontSize:20,marginLeft:10}}>Add Task</Text>
            <Content>
              <Form style={{marginTop:10}}>
                <Item>
                  <Input placeholder="Title" value={this.state.title}  onChangeText={(e) => this.setState({title: e})}
/>
                </Item>
                <Item last>
                  <Input placeholder="Description" />
                </Item>
                <Button style={{marginTop:10,width:400,marginLeft:5}} onPress={} primary><Text style={{marginLeft:165}}>Save</Text></Button>
              </Form>
            </Content>
      </Container>

        )
    }
}

export default Home;